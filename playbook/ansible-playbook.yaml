---
- hosts: localhost
  become: true
  #  ცვლადების ფაილის ადგილმდებარეობას ვუთითებთ რომ შემდგომში მოხდეს გამოყენება ცვლადების.
  vars_files:
    - vars/default.yml

  tasks:
    - name: Install prerequisites
      apt: name=aptitude update_cache=no state=latest force_apt_get=yes
      # ერთგვარი თაგირება.
      tags: [ system ]


    - name: Install LAMP Packages
      apt: name={{ item }} update_cache=no state=latest
      #  ლუპის საშვალებით გადაირბენს და items მიანიჭებს მოდულის სახელებს
      loop: [ 'apache2', 'mysql-server', 'python3-pymysql', 'php', 'php-mysql', 'libapache2-mod-php' ]
      tags: [ system ]

    - name: Install PHP Extensions
      apt: name={{ item }} update_cache=no state=latest
      # vars/default დან მოგვაქვს მოდულები. 
      loop: "{{ php_modules }}"
      tags: [ system ]

  # Apache Configuration
    - name: Create document root
      #  ამ მოდულის მეშვეობით შეგვიძლია შევქმნათ საქაღალდე.
      file:
        # გავჭერეთ მდებარეობა სად გვინდა ფოლდერის შექმნა და ცვლადი წამოვიღეთ ვარ საქაღალდედან
        path: "/var/www/{{ http_host }}"
        # აქ ვუთხარით რომ გვინდა 
        #  ეს მოდული ამოწმებს არსებობს თუ არა მიტითებულ მისამართზე ფაილი თუ არ არსებოსბს ის შექმნის
        state: directory
        owner: "www-data" # ფოლდერის მფლობელი
        group: "www-data" # მიტითებული სახელის ჯგუფში ჩამატება
        mode: '0755'   # პერსმიშენის მოდის მინიჭება 
      tags: [ apache ]

    - name: Set up Apache VirtualHost
    # ეს მოდული გამოიყენება მხოლოდ jinja გაფართოების კონფიგის დასარენდერებლად 
      template:
        # ვაკოპირებთ ფაილს des ზე მითითებულ ადგილას 
        src: "files/apache.conf.j2"
        dest: "/etc/apache2/sites-available/{{ http_conf }}"
        # ეს ვერ გავიგე კარგად
      notify: Reload Apache
      tags: [ apache ]

    - name: Enable rewrite module
      shell: /usr/sbin/a2enmod rewrite
      notify: Reload Apache
      tags: [ apache ]

    - name: Enable new site
      shell: /usr/sbin/a2ensite {{ http_conf }}
      notify: Reload Apache
      tags: [ apache ]

    - name: Disable default Apache site
      shell: /usr/sbin/a2dissite 000-default.conf
      notify: Restart Apache
      tags: [ apache ]

  # MySQL Configuration
    - name: Set the root password
      #  ეს მოდული გვეხმარება mydsql თან მარტივად ურთიერთობაში
      mysql_user:
        #  ანიჭებს root სახელს 
        name: root
        #  ესევე პაროლს აყენებს
        password: "{{ mysql_root_password }}"
        # დაუკავშირდება mysql სოკეტს და დალოგინდება მითითებული პაროლით და სახელით
        login_unix_socket: /var/run/mysqld/mysqld.sock
      tags: [ mysql, mysql-root ]

    - name: Remove all anonymous user accounts
      #  ეს მოდული გვეხმარება ქვემოთ მოცემული mysql მოდულების სამართავად მარტივად რომ ვთქვათ მომხმარებლების სამართავად გვეხმარება
      mysql_user:
        # მითითებულ სახელს წაშლის
        name: ''
        # ყველა მომხმარებელს წაშლის ჰოსტიდან
        host_all: yes
        #  ეს კარგად მაქ გასარკვევი ysql მოდულია
        state: absent
        #  დალოგინდება როგორ root იუზერი 
        login_user: root
        login_password: "{{ mysql_root_password }}"
      tags: [ mysql ]

    - name: Remove the MySQL test database
      # ეს მოდული გამოიყენება ბაზებთან ურთიერთობისთვის/სამართავად
      mysql_db:
        #  ეს განსაზღვრავს მონაცემთა ბაზის სახელს.
        name: test
        # თუ მიტითებული სახელით არსებობს მონაცემთა ბაზა ის ამოიღებს ამ მოდულის საშვალებით
        state: absent
        login_user: root
        login_password: "{{ mysql_root_password }}"
      tags: [ mysql ]

    - name: Creates database for WordPress
      mysql_db:
        # ვუთითებთ ბაზის სახელს.
        name: "{{ mysql_db }}"
        # ამ შემთხვევაში აჭმყო ისახავს ამ დროიდელ მდგომარეობას რაც მოიცავს რომ მითითებული სახელით ბაზა უნდა არსებობდეს 
        # თუ არ არსწბობს ის შექმნის
        state: present
        login_user: root
        login_password: "{{ mysql_root_password }}"
      tags: [ mysql ]

    - name: Create MySQL user for WordPress
      #  ეს მოდული გამოიყენება mysql მმხმერებლების სამართავად
      mysql_user:
        #  mysql მომხხმარებლის სახელი
        name: "{{ mysql_user }}"
        #  პაროლის დაყენება მომხმარებლისთვის
        password: "{{ mysql_password }}"
        # მომხმარებელს მითითებულ მონაცემთა ბაზაში ენიჭება პრივილეგია all ყველა ცხრილზე.
        priv: "{{ mysql_db }}.*:ALL"
        # ეს მოდული შექმნის
        state: present
        login_user: root
        login_password: "{{ mysql_root_password }}"
      tags: [ mysql ]

  # UFW Configuration
    - name: "UFW - Allow HTTP on port {{ http_port }}"
      # მოდული რომელიც გვეხმარება ფაირვოლთან საკომუნიკაცოტ
      ufw:
        # დაშვებას აკეთებს მითითებულ პორტზე
        rule: allow
        port: "{{ http_port }}"
        proto: tcp
      tags: [ system ]

  # WordPress Configuration
    - name: Download and unpack latest WordPress
      # მითითებული მოდული გვეხმარება ამოარქივებაში ფაილის
      unarchive:
        # მოგვაქვს ფაილი მითითებული ლინკიდან
        src: https://wordpress.org/latest.tar.gz
        # გადაგვაქ მითითებულ ადგილას
        dest: "/var/www/{{ http_host }}"
        remote_src: yes
        creates: "/var/www/{{ http_host }}/wordpress"
      tags: [ wordpress ]

    - name: Set ownership
      file:
        path: "/var/www/{{ http_host }}"
        state: directory
        recurse: yes
        owner: www-data
        group: www-data
      tags: [ wordpress ]

    - name: Set permissions for directories
      shell: "/usr/bin/find /var/www/{{ http_host }}/wordpress/ -type d -exec chmod 750 {} \\;"
      tags: [ wordpress ]

    - name: Set permissions for files
      shell: "/usr/bin/find /var/www/{{ http_host }}/wordpress/ -type f -exec chmod 640 {} \\;"
      tags: [ wordpress ]

    - name: Set up wp-config
      template:
        src: "files/wp-config.php.j2"
        dest: "/var/www/{{ http_host }}/wordpress/wp-config.php"
      tags: [ wordpress ]

  handlers:
    - name: Reload Apache
      service:
        name: apache2
        state: reloaded

    - name: Restart Apache
      service:
        name: apache2
        state: restarted